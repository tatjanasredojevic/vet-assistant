/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.tatjanasredojevic.vetassistant.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.tatjanasredojevic.vetassistant.model.ServiceType;
import rs.tatjanasredojevic.vetassistant.service.ServiceTypeService;

/**
 *
 * @author Tatjana
 */
@RestController
@RequestMapping("api/v1/service-types")
public class ServiceTypeController {
    
    private final ServiceTypeService serviceTypeService;

    @Autowired
    public ServiceTypeController(ServiceTypeService serviceTypeService) {
        this.serviceTypeService = serviceTypeService;
    }

   
    @PostMapping
    public ServiceType save(@RequestBody ServiceType serviceType){
        return serviceTypeService.save(serviceType);
    }
    @GetMapping
    public List<ServiceType> getAll(){
        return serviceTypeService.getAll();
    }
    
    @GetMapping("{id}")
    public ServiceType get(@PathVariable("id") Long id){
        return serviceTypeService.get(id);
    }
    
    @PutMapping("{id}")
    public ServiceType update(@PathVariable("id") Long id, @RequestBody ServiceType serviceType){
        return serviceTypeService.update(id, serviceType);
    }
    
    @DeleteMapping("{id}")
    public ServiceType delete(@PathVariable("id") Long id){
        return serviceTypeService.delete(id);
    }
   
}
