package rs.tatjanasredojevic.vetassistant.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.tatjanasredojevic.vetassistant.model.AnimalType;
import rs.tatjanasredojevic.vetassistant.service.AnimalTypeService;

/**
 *
 * @author Tatjana
 */
@RestController
@RequestMapping("api/v1/animal-types")
public class AnimalTypeController {
    
    private final AnimalTypeService animalTypeService;

    @Autowired
    public AnimalTypeController(AnimalTypeService animalTypeService) {
        this.animalTypeService = animalTypeService;
    }
    
    @PostMapping
    public AnimalType save(@RequestBody AnimalType animalType) {
        return animalTypeService.save(animalType);
    }
        
    @GetMapping
    public List<AnimalType> getAll() {
        return animalTypeService.getAll();
    }
    
    @GetMapping("{id}")
    public AnimalType get(@PathVariable("id") Long id) {
        return animalTypeService.get(id);
    }
    
    @PutMapping("{id}")
    public AnimalType update(@PathVariable("id") Long id, @RequestBody AnimalType animalType) {
        return animalTypeService.update(id, animalType);
    }
    
    @DeleteMapping("{id}")
    public AnimalType delete(@PathVariable("id") Long id) {
        return animalTypeService.delete(id);
    }
    
}
