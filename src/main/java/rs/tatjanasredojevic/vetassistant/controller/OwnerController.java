/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.tatjanasredojevic.vetassistant.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.tatjanasredojevic.vetassistant.model.Owner;
import rs.tatjanasredojevic.vetassistant.service.OwnerService;

/**
 *
 * @author Tatjana
 */
@RestController
@RequestMapping("api/v1/owner")
public class OwnerController {
    
    private final OwnerService ownerService;

    @Autowired
    public OwnerController(OwnerService ownerService) {
        this.ownerService = ownerService;
    }
    
    
    @PostMapping
    public Owner save(@RequestBody Owner owner){
        return ownerService.save(owner);
    }
    
    @GetMapping
    public List<Owner> getAll(){
        return ownerService.getAll();
    }
    
    @GetMapping("{id}")
    public Owner get(@PathVariable("id") Long id){
        return ownerService.get(id);
    }
    
    @DeleteMapping("{id}")
    public Owner delete(@PathVariable("id") Long id){
        return ownerService.delete(id);
    }
    
    @PutMapping("{id}")
    public Owner update(@PathVariable("id") Long id, @RequestBody Owner owner){
        return ownerService.update(id, owner);
    }

}