package rs.tatjanasredojevic.vetassistant.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.tatjanasredojevic.vetassistant.model.VetService;
import rs.tatjanasredojevic.vetassistant.service.VetServiceService;

/**
 *
 * @author Tatjana
 */
@RestController
@RequestMapping("api/v1/service")
public class VetServiceController {
   
    private final VetServiceService vetServiceService;

    @Autowired
    public VetServiceController(VetServiceService vetServiceService) {
        this.vetServiceService = vetServiceService;
    }
    
    @GetMapping
    public List<VetService> getAll(){
        return vetServiceService.getAll();
    }
    
    @GetMapping("{id}")
    public VetService get(@PathVariable("id") Long id){
        return vetServiceService.get(id);
    }
    
    @DeleteMapping("{id}")
    public VetService delete(@PathVariable("id") Long id){
        return vetServiceService.delete(id);
    }
    
    @PostMapping
    public VetService save(@RequestBody VetService service){
        return vetServiceService.save(service);
    }
    
    @PutMapping("{id}")
    public VetService update(@PathVariable("id") Long id, @RequestBody VetService service){
        return vetServiceService.update(id, service);
    }
    
}
