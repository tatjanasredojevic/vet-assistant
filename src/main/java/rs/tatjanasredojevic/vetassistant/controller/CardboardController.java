package rs.tatjanasredojevic.vetassistant.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.tatjanasredojevic.vetassistant.model.Cardboard;
import rs.tatjanasredojevic.vetassistant.service.CardboardService;

/**
 *
 * @author Tatjana
 */
@RestController
@RequestMapping("api/v1/cardboard")
public class CardboardController {
    
    private final CardboardService cardboardService;

    @Autowired
    public CardboardController(CardboardService cardboardService) {
        this.cardboardService = cardboardService;
    }
      
    @GetMapping
    public List<Cardboard> getAll(){
        return cardboardService.getAll();
    }
    
    @GetMapping("{id}")
    public Cardboard get(@PathVariable("id") Long id){
        return cardboardService.get(id);
    }

    @DeleteMapping("{id}")
    public Cardboard delete(@PathVariable("id") Long id){
        return cardboardService.delete(id);
    }
    
    @PostMapping
    public Cardboard save(@RequestBody Cardboard cardboard){
        return cardboardService.save(cardboard);
    }
    
    @PutMapping("{id}")
    public Cardboard update(@PathVariable("id") Long id, @RequestBody Cardboard cardboard){
        return cardboardService.update(id, cardboard);
    }
    
}
