/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.tatjanasredojevic.vetassistant.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.tatjanasredojevic.vetassistant.model.Intervention;
import rs.tatjanasredojevic.vetassistant.service.InterventionService;


/**
 *
 * @author Tatjana
 */
@RestController
    @RequestMapping("api/v1/cardboards/{cardboardId}/interventions")
public class InterventionController {
    
    private final InterventionService interventionService;

    @Autowired
    public InterventionController(InterventionService interventionService) {
        this.interventionService = interventionService;
    }
    
    @GetMapping
    public List<Intervention> getAll(@PathVariable("cardboardId") Long cardboardId){
        return interventionService.getAllByCardboardId(cardboardId);
    }
    
    @GetMapping("{id}")
    public Intervention get(@PathVariable("id") Long id){
        return interventionService.get(id);
    }
    
    @DeleteMapping("{id}")
    public Intervention delete(@PathVariable("id") Long id){
        return interventionService.delete(id);
    }
    
    @PostMapping
    public Intervention save(@RequestBody Intervention intervention){
        return interventionService.save(intervention);
    }
    
    @PutMapping("{id}")
    public Intervention update(@PathVariable("id") Long id, @RequestBody Intervention intervention){
        return interventionService.update(id, intervention);
    }
    
}
