/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.tatjanasredojevic.vetassistant.service;

import java.util.List;
import rs.tatjanasredojevic.vetassistant.model.ServiceType;

/**
 *
 * @author Tatjana
 */
public interface ServiceTypeService {
    ServiceType save(ServiceType serviceType);
    List<ServiceType> getAll();
    ServiceType get(Long id);
    ServiceType delete(Long id);
    ServiceType update(Long id, ServiceType serviceType);
}
