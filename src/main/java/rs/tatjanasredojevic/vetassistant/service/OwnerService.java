/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.tatjanasredojevic.vetassistant.service;

import java.util.List;
import rs.tatjanasredojevic.vetassistant.model.Owner;

/**
 *
 * @author Tatjana
 */
public interface OwnerService {
    Owner save(Owner owner);
    List<Owner> getAll();
    Owner get(Long id);
    Owner delete(Long id);
    Owner update(Long id, Owner owner);
}
