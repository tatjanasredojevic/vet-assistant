/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.tatjanasredojevic.vetassistant.service;

import java.util.List;
import rs.tatjanasredojevic.vetassistant.dto.LoginRequest;
import rs.tatjanasredojevic.vetassistant.model.User;

/**
 *
 * @author Tatjana
 */
public interface UserService {
    User save(User user);
    List<User> getAll();
    User get(Long id);
    User getByName(String name);
    User delete(Long id);
    User update(Long id, User user);

    User login(LoginRequest loginRequest);
}
