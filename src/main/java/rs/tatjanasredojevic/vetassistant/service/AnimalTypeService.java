package rs.tatjanasredojevic.vetassistant.service;

import java.util.List;
import rs.tatjanasredojevic.vetassistant.model.AnimalType;

/**
 *
 * @author Tatjana
 */
public interface AnimalTypeService {
    AnimalType save(AnimalType animalType);
    List<AnimalType> getAll();
    AnimalType get(Long id);
    AnimalType delete(Long id);
    AnimalType update(Long id, AnimalType animalType);
}
