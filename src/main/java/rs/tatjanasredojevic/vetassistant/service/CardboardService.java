/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.tatjanasredojevic.vetassistant.service;

import java.util.List;
import rs.tatjanasredojevic.vetassistant.model.Cardboard;

/**
 *
 * @author Tatjana
 */
public interface CardboardService {
    List<Cardboard> getAll();
    Cardboard get(Long id);
    Cardboard save(Cardboard cardboard);
    Cardboard delete(Long id);
    Cardboard update(Long id, Cardboard cardboard);  
    
}
