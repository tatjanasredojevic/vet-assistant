/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.tatjanasredojevic.vetassistant.service;

import java.util.List;
import rs.tatjanasredojevic.vetassistant.model.VetService;

/**
 *
 * @author Tatjana
 */
public interface VetServiceService {
    VetService save(VetService service);
    VetService get(Long id);
    List<VetService> getAll();
    VetService delete(Long id);
    VetService update(Long id, VetService service);
}
