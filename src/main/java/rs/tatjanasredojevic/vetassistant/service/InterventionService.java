package rs.tatjanasredojevic.vetassistant.service;

import java.util.List;
import rs.tatjanasredojevic.vetassistant.model.Intervention;

/**
 *
 * @author Tatjana
 */
public interface InterventionService {
    Intervention save(Intervention intervention);
    Intervention get(Long id);
    List<Intervention> getAll();
    List<Intervention> getAllByCardboardId(Long cardboardId);
    Intervention delete(Long id);
    Intervention update(Long id, Intervention intervention);
}
