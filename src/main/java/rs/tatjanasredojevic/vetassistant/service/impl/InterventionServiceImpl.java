package rs.tatjanasredojevic.vetassistant.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.tatjanasredojevic.vetassistant.model.Cardboard;
import rs.tatjanasredojevic.vetassistant.model.Intervention;
import rs.tatjanasredojevic.vetassistant.repository.CardboardRepository;
import rs.tatjanasredojevic.vetassistant.repository.InterventionRepository;
import rs.tatjanasredojevic.vetassistant.service.InterventionService;

/**
 *
 * @author Tatjana
 */
@Service
@Transactional
public class InterventionServiceImpl implements InterventionService{

    private final InterventionRepository interventionRepository;
    private final CardboardRepository cardboardRepository;

    @Autowired
    public InterventionServiceImpl(CardboardRepository cardboardRepository, InterventionRepository interventionRepository) {
        this.interventionRepository = interventionRepository;
        this.cardboardRepository = cardboardRepository;
    }

    @Override
    public Intervention save(Intervention intervention) {
        Cardboard cardboard = cardboardRepository.findById(intervention.getCardboard().getId())
                .orElseThrow(() -> new RuntimeException("cardborad does not exists"));
        intervention.setCardboard(cardboard);
        
        intervention.getServices().forEach(service -> {
            service.setIntervention(intervention);
        });
        
        return interventionRepository.save(intervention);
    }

    @Override
    public Intervention get(Long id) {
        return interventionRepository.findById(id).
                orElseThrow(() -> new RuntimeException("entity does not exists"));
    }

    @Override
    public List<Intervention> getAll() {
        List<Intervention> interventions = new ArrayList<>();
        interventionRepository.findAll().forEach(intervention -> interventions.add(intervention));
        return interventions;
    }

    @Override
    public List<Intervention> getAllByCardboardId(Long cardboardId) {
        return interventionRepository.getAllByCardboardId(cardboardId);
    }

    @Override
    public Intervention delete(Long id) {
        Intervention intervention = get(id);
        interventionRepository.deleteById(id);
        return intervention;
    }

    @Override
    public Intervention update(Long id, Intervention intervention) {
        Intervention fetchedIntervention = get(id);
        intervention.getServices().forEach(service -> {
            service.setIntervention(fetchedIntervention);
        });
        fetchedIntervention.setDescription(intervention.getDescription());
        fetchedIntervention.setCreatedDate(intervention.getCreatedDate());
        fetchedIntervention.getServices().clear();
        fetchedIntervention.getServices().addAll(intervention.getServices());
        
        interventionRepository.save(fetchedIntervention);
        return fetchedIntervention;
    }
}
