/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.tatjanasredojevic.vetassistant.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.tatjanasredojevic.vetassistant.dto.LoginRequest;
import rs.tatjanasredojevic.vetassistant.model.User;
import rs.tatjanasredojevic.vetassistant.repository.UserRepository;
import rs.tatjanasredojevic.vetassistant.service.UserService;

/**
 *
 * @author Tatjana
 */
@Service
@Transactional
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(user -> users.add(user));
        return users;
    }

    @Override
    public User get(Long id) {
        return userRepository.findById(id)
                             .orElseThrow(() -> new RuntimeException("entity does not exists"));
    }
    
    
    @Override
    public User getByName(String username) {
        return userRepository.findFirstByUsername(username)
                .orElseThrow(() -> new RuntimeException("entity does not exists"));
    }

    @Override
    public User delete(Long id) {
        User user = get(id);
        userRepository.deleteById(id);
        return user;
    }

    @Override
    public User update(Long id, User user) {
        User fetchedUser = get(id);
        fetchedUser.setAddress(user.getAddress());
        fetchedUser.setBirthday(user.getBirthday());
        fetchedUser.setEmail(user.getEmail());
        fetchedUser.setLastname(user.getLastname());
        fetchedUser.setName(user.getName());
        fetchedUser.setPassword(user.getPassword());
        fetchedUser.setPhoneNumber(user.getPhoneNumber());
        fetchedUser.setUsername(user.getUsername());
        return userRepository.save(fetchedUser);
    }

    @Override
    public User login(LoginRequest loginRequest) {
        return userRepository.findFirstByUsernameAndPassword(loginRequest.getUsername(),
                                                             loginRequest.getPassword())
                .orElseThrow(() -> new RuntimeException("User ne posotoji"));
    }
}
