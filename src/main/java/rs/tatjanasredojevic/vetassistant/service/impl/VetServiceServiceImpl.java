package rs.tatjanasredojevic.vetassistant.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.tatjanasredojevic.vetassistant.model.Intervention;
import rs.tatjanasredojevic.vetassistant.model.ServiceType;
import rs.tatjanasredojevic.vetassistant.model.User;
import rs.tatjanasredojevic.vetassistant.model.VetService;
import rs.tatjanasredojevic.vetassistant.repository.InterventionRepository;
import rs.tatjanasredojevic.vetassistant.repository.ServiceRepository;
import rs.tatjanasredojevic.vetassistant.repository.ServiceTypeRepository;
import rs.tatjanasredojevic.vetassistant.repository.UserRepository;
import rs.tatjanasredojevic.vetassistant.service.VetServiceService;

/**
 *
 * @author Tatjana
 */
@Service
@Transactional
public class VetServiceServiceImpl implements VetServiceService{

    private final ServiceRepository serviceRepository;
    private final ServiceTypeRepository serviceTypeRepository;
    private final UserRepository userRepository;
    private final InterventionRepository interventionRepository;

    @Autowired
    public VetServiceServiceImpl(ServiceRepository serviceRepository,
            ServiceTypeRepository serviceTypeRepository,
            UserRepository userRepository,
            InterventionRepository interventionRepository) {
        this.serviceRepository = serviceRepository;
        this.serviceTypeRepository = serviceTypeRepository;
        this.userRepository = userRepository;
        this.interventionRepository = interventionRepository;
    }

    @Override
    public VetService save(VetService service) {
        service.setIntervention(getIntervention(service.getIntervention().getId()));
        service.setServiceType(getServiceType(service.getServiceType().getId()));
        service.setUser(getUser(service.getUser().getId()));
        return serviceRepository.save(service);
    }

    @Override
    public VetService get(Long id) {
        return serviceRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("entity does not exists"));
    }

    @Override
    public List<VetService> getAll() {
        List<VetService> vetServices = new ArrayList<>();
        serviceRepository.findAll().forEach(vetService -> vetServices.add(vetService));
        return vetServices;
    }

    @Override
    public VetService delete(Long id) {
        VetService service = get(id);
        serviceRepository.deleteById(id);
        return service;
    }

    @Override
    public VetService update(Long id, VetService service) {
        VetService fetchedService = get(id);
        ServiceType serviceType = getServiceType(service.getServiceType().getId());
        User user = getUser(service.getUser().getId());

        fetchedService.setDescription(service.getDescription());
        fetchedService.setServiceType(serviceType);
        fetchedService.setUser(user);

        serviceRepository.save(fetchedService);
        return fetchedService;
    }

    private ServiceType getServiceType(Long id) {
        return serviceTypeRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("service type does not exists"));
    }

    private User getUser(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("user does not exists"));
    }

    private Intervention getIntervention(Long interventionId) {
        return interventionRepository
                .findById(interventionId)
                .orElseThrow(() -> new RuntimeException("intervention does not exits!"));
    }

}
