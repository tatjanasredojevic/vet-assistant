/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.tatjanasredojevic.vetassistant.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.tatjanasredojevic.vetassistant.model.ServiceType;
import rs.tatjanasredojevic.vetassistant.repository.ServiceTypeRepository;
import rs.tatjanasredojevic.vetassistant.service.ServiceTypeService;

/**
 *
 * @author Tatjana
 */
@Service
@Transactional
public class ServiceTypeServiceImpl implements ServiceTypeService{

    private final ServiceTypeRepository serviceTypeRepository;

    @Autowired
    public ServiceTypeServiceImpl(ServiceTypeRepository serviceTypeRepository) {
        this.serviceTypeRepository = serviceTypeRepository;
    }

    @Override
    public ServiceType save(ServiceType serviceType) {
        return serviceTypeRepository.save(serviceType);
    }

    @Override
    public List<ServiceType> getAll() {
        List<ServiceType> serviceTypes = new ArrayList<>();
        serviceTypeRepository.findAll().forEach(serviceType -> serviceTypes.add(serviceType));
        return serviceTypes;
    }

    @Override
    public ServiceType get(Long id) {
        return serviceTypeRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("entity does not exists"));
    }

    @Override
    public ServiceType delete(Long id) {
        ServiceType serviceType = get(id);
        serviceTypeRepository.deleteById(id);
        return serviceType;
    }

    @Override
    public ServiceType update(Long id, ServiceType serviceType) {
        ServiceType fetchedServiceType = get(id);
        fetchedServiceType.setDescription(serviceType.getDescription());
        fetchedServiceType.setMarking(serviceType.getMarking());
        fetchedServiceType.setName(serviceType.getName());
        return serviceTypeRepository.save(fetchedServiceType);
    }
}
