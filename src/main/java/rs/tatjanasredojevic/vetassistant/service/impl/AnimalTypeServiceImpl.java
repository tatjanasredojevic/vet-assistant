package rs.tatjanasredojevic.vetassistant.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.tatjanasredojevic.vetassistant.model.AnimalType;
import rs.tatjanasredojevic.vetassistant.repository.AnimalTypeRepository;
import rs.tatjanasredojevic.vetassistant.service.AnimalTypeService;

/**
 *
 * @author Tatjana
 */
@Service
@Transactional
public class AnimalTypeServiceImpl implements AnimalTypeService {

    private final AnimalTypeRepository animalTypeRepository;

    @Autowired
    public AnimalTypeServiceImpl(AnimalTypeRepository animalTypeRepository) {
        this.animalTypeRepository = animalTypeRepository;
    }

    @Override
    public AnimalType save(AnimalType animalType) {
        return animalTypeRepository.save(animalType);
    }

    @Override
    public List<AnimalType> getAll() {
        List<AnimalType> animalTypes = new ArrayList<>();
        animalTypeRepository.findAll().forEach(animalType -> animalTypes.add(animalType));
        return animalTypes;
    }

    @Override
    public AnimalType get(Long id) {
        return animalTypeRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("entity does not exists"));
    }

    @Override
    public AnimalType delete(Long id) {
        AnimalType animalType = get(id);
        animalTypeRepository.deleteById(id);
        return animalType;
    }

    @Override
    public AnimalType update(Long id, AnimalType animalType) {
        AnimalType fetchedAnimalType = get(id);
        fetchedAnimalType.setDescription(animalType.getDescription());
        fetchedAnimalType.setName(animalType.getName());
        return animalTypeRepository.save(fetchedAnimalType);
    }
}
