package rs.tatjanasredojevic.vetassistant.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.tatjanasredojevic.vetassistant.model.AnimalType;
import rs.tatjanasredojevic.vetassistant.model.Cardboard;
import rs.tatjanasredojevic.vetassistant.model.Intervention;
import rs.tatjanasredojevic.vetassistant.model.Owner;
import rs.tatjanasredojevic.vetassistant.repository.AnimalTypeRepository;
import rs.tatjanasredojevic.vetassistant.repository.CardboardRepository;
import rs.tatjanasredojevic.vetassistant.repository.OwnerRepository;
import rs.tatjanasredojevic.vetassistant.service.CardboardService;

/**
 *
 * @author Tatjana
 */
@Service
@Transactional
public class CardboardServiceImpl implements CardboardService{

    private final CardboardRepository cardboardRepository;
    private final OwnerRepository ownerRepository;
    private final AnimalTypeRepository animalTypeRepository;

    @Autowired
    public CardboardServiceImpl(CardboardRepository cardboardRepository,
            OwnerRepository ownerRepository,
            AnimalTypeRepository animalTypeRepository) {
        this.cardboardRepository = cardboardRepository;
        this.ownerRepository = ownerRepository;
        this.animalTypeRepository = animalTypeRepository;
    }
    
    @Override
    public List<Cardboard> getAll() {
        List<Cardboard> cardboards = new ArrayList<>();
        cardboardRepository.findAll().forEach(cardboards::add);
        return cardboards;
    }
    @Override
    public Cardboard get(Long id) {
        Cardboard cardboard = cardboardRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("entity does not exists"));
        
        cardboard.getInterventions().forEach((i) -> {
            i.getServices();
        });
        
        return cardboard;
    }

    @Override
    public Cardboard save(Cardboard cardboard) {
        Owner owner = getOwner(cardboard.getOwner().getId());
        AnimalType animalType = getAnimalType(cardboard.getAnimalType().getId());

        cardboard.setOwner(owner);
        cardboard.setAnimalType(animalType);

        return cardboardRepository.save(cardboard);
    }

    @Override
    public Cardboard delete(Long id) {
        Cardboard cardboard = get(id);
        cardboardRepository.deleteById(id);
        return cardboard;
    }

    @Override
    public Cardboard update(Long id, Cardboard cardboard) {
        Cardboard fetchedCardboard = get(id);
        Owner owner = getOwner(cardboard.getOwner().getId());
        AnimalType animalType = getAnimalType(cardboard.getAnimalType().getId());

        fetchedCardboard.setName(cardboard.getName());
        fetchedCardboard.setBirthday(cardboard.getBirthday());
        fetchedCardboard.setCreatedDate(cardboard.getCreatedDate());
        fetchedCardboard.setDeactivated(cardboard.isDeactivated());

        fetchedCardboard.setOwner(owner);
        fetchedCardboard.setAnimalType(animalType);

        cardboardRepository.save(cardboard);
        return fetchedCardboard;
    }

    private Owner getOwner(Long id) {
        return ownerRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("owner does not exists"));
    }

    private AnimalType getAnimalType(Long id) {
        return animalTypeRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("animal type does not exists"));
    }
}
