/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.tatjanasredojevic.vetassistant.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rs.tatjanasredojevic.vetassistant.model.Owner;
import rs.tatjanasredojevic.vetassistant.repository.OwnerRepository;
import rs.tatjanasredojevic.vetassistant.service.OwnerService;

/**
 *
 * @author Tatjana
 */
@Service
@Transactional
public class OwnerServiceImpl implements OwnerService{

    private final OwnerRepository ownerRepository;

    @Autowired
    public OwnerServiceImpl(OwnerRepository ownerRepository) {
        this.ownerRepository = ownerRepository;
    }

    @Override
    public Owner save(Owner owner) {
        return ownerRepository.save(owner);
    }

    @Override
    public List<Owner> getAll() {
        List<Owner> owners = new ArrayList<>();
        ownerRepository.findAll().forEach(owner -> owners.add(owner));
        return owners;
    }

    @Override
    public Owner get(Long id) {
        return ownerRepository.findById(id).orElseThrow(() -> new RuntimeException("entity does not exists"));
    }

    @Override
    public Owner delete(Long id) {
        Owner owner = get(id);
        ownerRepository.deleteById(id);
        return owner;
    }

    @Override
    public Owner update(Long id, Owner owner) {
        Owner fetchedOwner = get(id);

        fetchedOwner.setAddress(owner.getAddress());
        fetchedOwner.setBirthday(owner.getBirthday());
        fetchedOwner.setEmail(owner.getEmail());
        fetchedOwner.setLastname(owner.getLastname());
        fetchedOwner.setName(owner.getName());
        fetchedOwner.setPhoneNumber(owner.getPhoneNumber());

        return ownerRepository.save(fetchedOwner);
    }
}
