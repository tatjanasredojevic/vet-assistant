package rs.tatjanasredojevic.vetassistant.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import rs.tatjanasredojevic.vetassistant.model.Intervention;

/**
 *
 * @author Tatjana
 */
@Repository
public interface InterventionRepository extends CrudRepository<Intervention, Long>{
 
    List<Intervention> getAllByCardboardId(Long cardboardId);
    
}
