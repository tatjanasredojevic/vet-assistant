/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.tatjanasredojevic.vetassistant.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import rs.tatjanasredojevic.vetassistant.model.Owner;

/**
 *
 * @author Tatjana
 */
@Repository
public interface OwnerRepository extends CrudRepository<Owner, Long>{
    
}
