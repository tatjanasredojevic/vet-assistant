package rs.tatjanasredojevic.vetassistant.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import rs.tatjanasredojevic.vetassistant.model.AnimalType;

/**
 *
 * @author Tatjana
 */
@Repository
public interface AnimalTypeRepository extends CrudRepository<AnimalType, Long> {
    
}
