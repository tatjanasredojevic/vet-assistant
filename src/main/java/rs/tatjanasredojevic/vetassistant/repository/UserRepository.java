package rs.tatjanasredojevic.vetassistant.repository;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import rs.tatjanasredojevic.vetassistant.model.User;

/**
 *
 * @author Tatjana
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long>{
    
    Optional<User> findFirstByUsername(String username);
    Optional<User> findFirstByUsernameAndPassword(String username, String password);
    
}
