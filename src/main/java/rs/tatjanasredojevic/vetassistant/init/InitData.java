package rs.tatjanasredojevic.vetassistant.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import rs.tatjanasredojevic.vetassistant.model.AnimalType;
import rs.tatjanasredojevic.vetassistant.service.impl.AnimalTypeServiceImpl;

/**
 *
 * @author Tatjana
 */
@Component
public class InitData implements ApplicationListener<ContextRefreshedEvent> {
    
    private final AnimalTypeServiceImpl animalTypeService;

    @Autowired
    public InitData(AnimalTypeServiceImpl animalTypeService) {
        this.animalTypeService = animalTypeService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent e) {
         animalTypeService.save(new AnimalType("cat", "mnjauuuu"));
         animalTypeService.save(new AnimalType("dog", "avav"));
    }
    
}
