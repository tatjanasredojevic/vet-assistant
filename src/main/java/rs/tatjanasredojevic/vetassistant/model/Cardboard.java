package rs.tatjanasredojevic.vetassistant.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Tatjana
 */
@Entity
public class Cardboard implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    
    private String name;
    
    @Temporal(TemporalType.DATE)
    private Date birthday;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    
    private boolean deactivated;
    
    @ManyToOne
    private AnimalType animalType;
    
    @ManyToOne
    private Owner owner;
    
    @OneToMany(cascade = CascadeType.ALL,
               orphanRemoval = true,
               mappedBy = "cardboard",
               fetch = FetchType.LAZY)
    //@JoinColumn(name = "cardboard_id")
    private List<Intervention> interventions = new ArrayList<>();

    public Cardboard() {
    }
    

    public Cardboard(Long id, String name, Date birthday, Date createdDate, boolean deactivated, AnimalType animalType, Owner owner) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
        this.createdDate = createdDate;
        this.deactivated = deactivated;
        this.animalType = animalType;
        this.owner = owner;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isDeactivated() {
        return deactivated;
    }

    public void setDeactivated(boolean deactivated) {
        this.deactivated = deactivated;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }

    public void setAnimalType(AnimalType animalType) {
        this.animalType = animalType;
    }

    public List<Intervention> getInterventions() {
        return interventions;
    }

    public void setInterventions(List<Intervention> interventions) {
        this.interventions = interventions;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cardboard other = (Cardboard) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cardboard{" + "id=" + id + ", name=" + name + ", birthday=" + birthday + ", createdDate=" + createdDate + ", deactivated=" + deactivated + ", animalType=" + animalType + ", owner=" + owner + ", interventions=" + interventions + '}';
    }

}
