package rs.tatjanasredojevic.vetassistant.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Tatjana
 */
@Entity
public class Intervention implements Serializable{

    @Id
    @GeneratedValue
    private Long id;
    
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Cardboard cardboard;
    
    private String description;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @OneToMany(cascade = CascadeType.ALL,
               orphanRemoval = true,
               mappedBy = "intervention",
               fetch = FetchType.EAGER)
    private List<VetService> services;
    
    public Intervention() {
    }

    public Intervention(Long id, String description, Date createdDate) {
        this.id = id;
        this.description = description;
        this.createdDate = createdDate;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Cardboard getCardboard() {
        return cardboard;
    }

    public void setCardboard(Cardboard cardboard) {
        this.cardboard = cardboard;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Intervention other = (Intervention) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public List<VetService> getServices() {
        return services;
    }

    public void setServices(List<VetService> services) {
        this.services = services;
    }

    @Override
    public String toString() {
        return "Intervention{" + "id=" + id + ", cardboard=" + cardboard + ", description=" + description + ", createdDate=" + createdDate + ", services=" + services + '}';
    }

}
