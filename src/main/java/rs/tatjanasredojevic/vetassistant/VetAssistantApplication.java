package rs.tatjanasredojevic.vetassistant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VetAssistantApplication {

    
    public static void main(String[] args) {
        SpringApplication.run(VetAssistantApplication.class, args);
    }
}
